#include <iostream>
#include <vector>

using namespace std;

int dfs(const vector<vector<int>> &a, int v, vector<int> &used,
         vector<int> &sorted) {
    used[v] = 1; 
    for (size_t i = 0; i < a[v].size(); i++) {
        if (used[a[v][i]] == 0) {
            if (dfs(a, a[v][i], used, sorted)) {
                return 1;
            }
        }
        if (used[a[v][i]] == 1) {
            return 1;
        }
    }
    sorted.push_back(v);
    used[v] = -1;
    return 0;
}

int topological_sort(const vector<vector<int>> &a, vector<int> &used,
                     vector<int> &sorted) {
    for (size_t i = 0; i < used.size(); i++) {
        if (!used[i]) {
            if (dfs(a, i, used, sorted) == 1) {
                return 0;
            }
        }
    }
    return 1;    
}

int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>> vertex(n);
    vector<int> used(n, 0);
    int a, b;
    for (int i = 0; i < m; i++) {
        cin >> a >> b;
        vertex[a - 1].push_back(b - 1);
    } 
    vector<int> new_vector;
    if (topological_sort(vertex, used, new_vector) == 0) {
        cout << "No" << endl;
    } else {
        cout << "Yes" << endl;
        vector<int> vec(new_vector.rbegin(), new_vector.rend());   
        for (size_t i = 0; i < vec.size(); i++) {
            cout << vec[i] + 1 << ' ';
        }
        cout << endl; 
    }
    return 0;
}
