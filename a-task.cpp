#include <iostream>
#include <vector>

using namespace std;

void dfs(const vector<vector<int>> &A, int V, vector<int> &Used) {
    Used[V] = 1; 
    for (size_t i = 0; i < A[i].size(); i++) {
        if (A[V][i] && (!Used[i])) {
            dfs(A, i, Used);
        }
    }
}

int main() {
    int N, S;
    int Count = 0;
    cin >> N >> S;
    vector<vector<int>> A(N, vector<int>(N, 0));
    vector<int> Used(N, 0);
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cin >> A[i][j];
        }
    } 
    dfs(A, S - 1, Used);
    for(int i = 0; i < N; i++) {
        if (Used[i]) {
            Count++;
        }
    }
    cout << Count << endl; 
    return 0;
}
