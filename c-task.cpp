#include <iostream>
#include <vector>

using namespace std;

int dfs(const vector<vector<int>> &a, int v, vector<int> &used) {
    used[v] = 1;
    for (size_t i = 0; i < a[i].size(); i++) {
        if (a[v][i]) {
            if (used[i] == 0) {
                if (dfs(a, i, used)) {
                    return -1;
                }
            } 
            if (used[i] == 1) {
                return 1;
            }
        }
    }
    used[v] = -1;
    return 0;
}

int main() {
    int n;
    cin >> n;
    vector<vector<int>> a(n, vector<int>(n, 0));
    vector<int> used(n, 0);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> a[i][j];
        }
    } 
    for (int i = 0; i < n; i++) {
        if (dfs(a, i, used) == 1) {
            cout << 1 << endl;
            return 0;
        }
    }
    cout << 0 << endl;
    return 0;
}
